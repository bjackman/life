var SIZE_X, SIZE_Y;
var SIZE = 10;
SIZE_X = SIZE_Y = SIZE;

function showGrid(table, grid) {
  table.childNodes.forEach(function(row, y, rows) {
    row.childNodes.forEach(function(td, x, tds) {
      // I Love Dynamic Typing
      // check for undefined.
      if (!grid[y][x] && grid[y][x] !== false) {
        console.log("OH NOOOOOOES");
      }
      td.children[0].checked = grid[y][x];
    })
  })
}

function readGrid(table) {
  var grid = [];
  table.childNodes.forEach(function(row, y, rows) {
    grid[y] = [];
    row.childNodes.forEach(function(td, x, tds) {
      grid[y][x] = td.children[0].checked;
    })
  })
  return grid;
}

// initialise <table> using DOMpteur
// (totally worth the include - it's only 500 lines including comments)

var table = $("#main").addAsFirst({
  tag: "table",
});
for (var y = 0; y < SIZE_Y; y++) {
  var row = table.addAsLast({
    tag: 'tr'
  })
  for (var x = 0; x < SIZE_X; x++) {
    var td = row.addAsLast({
      tag: 'td',
      html: '<input type="checkbox" />'
    });
  }
};

var generations = [];

document.getElementById("next").onclick = function() {
  var currentState = readGrid(table);
  generations.push(currentState);
  showGrid(table, nextState(currentState));
}

document.getElementById("prev").onclick = function() {
  showGrid(table, generations.pop());
}