function numAdjacentChecked(grid, y, x) {
  var ret = 0;

  for (dY = -1; dY <= 1; dY++) {
    for (dX = -1; dX <= 1; dX++) {
      var row = grid[y + dY];
      if (row) {
        var other = row[x + dX];
      }
      if (row && other && other) {
        ret++;
      }
    }
  }
  // lazy caaaaaant
  if (grid[y][x]) ret--;
  return ret;
}

function nextState (prev) {
  var next = []
  for (var y = 0; y < prev.length; y++) {
    next[y] = []
    for (var x = 0; x < prev[y].length; x++) {
      var neighbours = numAdjacentChecked(prev, y, x);
      next[y][x] = neighbours == 3 ||
                   (prev[y][x] && neighbours == 2);
    }
  }
  console.log(next);
  return next;
}